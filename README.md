# INT-SELECTOR #

int-selector is an angular directive, that allow to select int values, and int ranges, like you do when you select page that you want to print.

```javascript
1; 3-5 // means select [1,3,4,5]
```
This directive is perfect to select pages of course, but also years.


### Usage ###

* Installation

```shell
npm install int-selector -save
```

* Add file to your build (webpack, gulp...)

```shell
node_modules/int-selector/dist/intselector.js
node_modules/int-selector/dist/intselector.css
```

* Dependencies

  * angularjs 1.x
  * bootstrap 3.x
  * lodash

* Configuration

1. Add module to your angular application

```javascript
  angular.module("YourApp", [..., 'int-selector', ...]);
```

2. Add directive to your html

```html
<int-selector 
	name="string" 
	placeholder="string"
	labels="object"

	separator="char"

	separators="string"
	ng-model="expression" 

	ngSuggests="expression"
	fixed="integer" 
	min="integer" 
	max="integer" 
	reverse="boolean"

	ng-disabled="expression" disabled="boolean"
	ng-readonly="expression" readonly="boolean"
	ng-required="expression" required="boolean"

	ng-change="expression"
	
	key-nav="expression">
</int-selector>
```

3. Options

* **name** : the name used in form, you can use it for get the validity of input form.inputname.$valid : default value : **undefined**
* **ng-model** : the integers array result. : default value : **[]**
* **min** : Min value accepted. : default value : 0
* **max** : Max value accepted. : default value : Number.MAX_SAFE_INTEGER : 9007199254740991
* **ng-suggests** : Values in dropdown menu : default value : []
* **ng-disabled**/**disabled** : Disable input, input value will be hide, it's different of readonly. : default value : **false**
* **ng-readonly**/**readonly** : Readonly input. : default value : **false**
* **ng-required**/**required** : Empty enter is not accepted. : default value : **false**
* **fixed** : Size of each input, usefull for years for example. : default value : **undefined**
* **separator** : Int separator. : default value : '**;**'
* **separators** : Int separators accepted from keyboard. : default value : '**;**'
* **key-nav** : Add keys navigation feature, ArrowUp, ArrowDown, Add(+) : default value : **true**
* **ng-change** : handler when value change. function receives two arguments, data (current list of int) and old, (previous list of int) : handlerChange(data, old). : default value : **undefined**
* **placeholder** : placeholder text : default value : '**Enter values or ranges, separed by comma...**'
* **labels** : all and none text : default value : '{all:'All', none:'None'}'

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';

import angular from 'angular';

import './intselector.css';
import './intselector.js';

//var angular = require('angular');
(function (ng, __) {
	'use strict';
	ng.module('app', ['int-selector']).controller('AppCtrl', AppCtrl);
	function AppCtrl() {
		var ctrl = this;
		ctrl.modelPages = function() {
			ctrl.suggests = _.range(0, 20).sort(function(a, b) {return a-b;});
			ctrl.min = 0;
			ctrl.max = 30;
			ctrl.fixed = 0;
			ctrl.selectedValues = [0, 5,6];
		};
		ctrl.modelYears = function() {
			ctrl.suggests = _.range(1995, 2005).sort(function(a, b) {return a-b;});
			ctrl.min = 1990;
			ctrl.max = 2010;
			ctrl.fixed = 4;
			ctrl.selectedValues = [2000, 2002,2003];
		};
		ctrl.lowerBound = 0;
		ctrl.upperBound = 30;
		ctrl.suggests = _.range(0, 20).sort(function(a, b) {return a-b;});
		ctrl.min = 0;
		ctrl.max = 20;
		ctrl.required = false;
		ctrl.disabled = false;
		ctrl.readonly = false;
		ctrl.keyNav = true;
		ctrl.events = "";
		ctrl.fixed = 0;
		ctrl.separator = "";
		ctrl.separators = ";,";
		ctrl.changeHandler = changeHandler;
		ctrl.selectedValues = [1, 2, 4, 5, 6];
		ctrl.dropmenu = true;
		ctrl.reverse = true;
		ctrl.size = null;

		function changeHandler(data, old) {
			ctrl.events = "Change values : " + old + " to " + data + "\n" + ctrl.events;
		}
	}
})(angular, _);

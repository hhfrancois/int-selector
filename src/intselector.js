(function (ng, __) {
	'use strict';
	ng.module('int-selector', []).run(config).directive('intSelector', intSelector).filter('intstoranges', intsToRanges).filter('without', without);
	/* @ngInject */
	function config($templateCache) {
		$templateCache.put('intselector.html', require('./intselector.html'));
	}
	/* @ngInject */
	function intSelector($filter) {
		return {
			restrict: 'E',
			templateUrl: 'intselector.html',
			controller: IntSelectorCtrl,
			controllerAs: 'ctrl',
			scope: {
				// nom interne : nom externe
				'ngModel': '=', // la valeur àditée
				'ngSuggests': '=', // array de suggestion
				'ngChange': "&", // levé uniquement quand une valeur valide est saisie

				'keyNav': "=", // active/desactive les touche fleche bas, fleche haut et +
				'labels': "=",

				'reverse': "@", // change le sens des int dans le dropmenu
				'inputname': "@name", // nom de l'input dans le formulaire
				'placeholder': "@", // texte si input vide
				'required': "@", // une valeur est elle requise
				'readonly': "@", // lecture seule
				'disabled': "@", // une valeur est elle requise
				'fixed': "@", // les entiers doivent ils etre formatés sur x chiffres, cas des années
				'min': "@", // la valeur min pouvant etre saisie
				'max': "@", // la valeur max pouvant etre saisie
				'separator': "@", // caractere de séparation par defaut le comma ;
				'separators': "@" // caractere de séparation par defaut le comma ;
			},
			link: function (scope, elm, attrs, ctrl) {
				var watcherClear;
				var watcherClears = [];
				watcherClear = scope.$watch('disabled', function (v1, v2, s) {
					updateDisabled(s.ctrl, v1);
				}, true);
				watcherClears.push(watcherClear);
				watcherClear = scope.$watch('readonly', function (v1, v2, s) {
					updateReadonly(s.ctrl, v1);
				}, true);
				watcherClears.push(watcherClear);
				watcherClear = scope.$watch('required', function (v1, v2, s) {
					updateRequired(s.ctrl, v1);
				}, true);
				watcherClears.push(watcherClear);
				watcherClear = scope.$watch('ngSuggests', function (v1, v2, s) {
					updateNgSuggests(s.ctrl, v1);
				}, true);
				watcherClears.push(watcherClear);
				watcherClear = scope.$watchGroup(['min', 'max'], function (vs1, vs2, s) {
					if (!arraysEqual(vs1, vs2)) {
						updateMinOrMax(s.ctrl, vs1[0], vs1[1])
					}
					;
				}, true);
				watcherClears.push(watcherClear);
				watcherClear = scope.$watchGroup(['fixed', 'separator', 'separators'], function (vs1, vs2, s) {
					if (!arraysEqual(vs1, vs2)) {
						updateFixedOrSep(s.ctrl, vs1[0], vs1[1], vs1[2])
					}
					;
				}, true);
				watcherClears.push(watcherClear);
				watcherClear = scope.$watchGroup(['ngModel', 'fixed', 'separator'], function (vs1, vs2, s) {
					if (!arraysEqual(vs1, vs2)) {
						updateNgModelOrFixedOrSep(s.ctrl, vs1[0], vs1[1], vs1[2])
					}
					;
				}, true);
				watcherClears.push(watcherClear);
				scope.$on('$destroy', function () {
					//stop watching when scope is destroyed
					watcherClears.forEach(function (watcherClear) {
						watcherClear();
					});
				});
				if (!scope.ngModel) {
					scope.ngModel = [];
				}
				updateMinOrMax(ctrl, scope.min, scope.max);
				updateFixedOrSep(ctrl, scope.fixed, scope.separator, scope.separators);
				updateNgModelOrFixedOrSep(ctrl, scope.ngModel, scope.fixed, scope.separator);
				updateNgSuggests(ctrl, scope.ngSuggests);
				updateDisabled(ctrl, scope.disabled);
				updateRequired(ctrl, scope.required);
				updateReadonly(ctrl, scope.readonly);
			}
		};
		function _parseInt(str, def) {
			var integer = parseInt(str);
			return isNaN(integer) ? def : integer;
		}
		function updateDisabled(ctrl, disabled) {
			ctrl.disabled = disabled;
		}
		function updateRequired(ctrl, required) {
			ctrl.required = required;
		}
		function updateNgSuggests(ctrl, ngSuggests) {
			var suggests = _.isArray(ngSuggests) ? ngSuggests.sort() : [];
			ctrl.values = suggests;
		}
		function updateReadonly(ctrl, readonly) {
			ctrl.readonly = readonly;
		}
		function updateMinOrMax(ctrl, min, max) {
			ctrl.min = _parseInt(min, 0);
			ctrl.max = _parseInt(max, Number.MAX_SAFE_INTEGER);
		}
		function updateFixedOrSep(ctrl, fixed, sep, seps) {
			ctrl.fixed = _parseInt(fixed);
			updateSep(ctrl, sep);
			updateSeps(ctrl, seps);
			var pattern = "\\d" + (ctrl.fixed ? "{" + ctrl.fixed + "}" : "+");
			ctrl.regex = new RegExp("^" + pattern + "(-" + pattern + ")?(\\s*[" + ctrl.separators + "]\\s*" + pattern + "(-" + pattern + ")?)*\\s*$");
			ctrl.regexinput = new RegExp("[\\d" + ctrl.separators + "-\\s]|Subtract|ArrowLeft|Left|ArrowRight|Right|Backspace|Tab|Delete|Del|Home|End");
		}
		function updateNgModelOrFixedOrSep(ctrl, ngModel, fixed, sep) {
			updateSep(ctrl, sep);
			ctrl.userinput = $filter("intstoranges")(ngModel, _parseInt(fixed), ctrl.separator);
		}
		function updateSep(ctrl, sep) {
			ctrl.separator = sep && sep.length === 1 ? sep : ";";
		}
		function updateSeps(ctrl, seps) {
			ctrl.separators = seps.indexOf(ctrl.separator) !== -1 ? seps : seps + ctrl.separator;
		}
	}
	/* @ngInject */
	function IntSelectorCtrl($scope, $filter) {
		var ctrl = this;
		ctrl.regex;
		ctrl.pattern = '.*';
		ctrl.regexinput;
		ctrl.userinput;
		ctrl.values = [];
		ctrl.separator = ";";
		ctrl.separators = ";";
		ctrl.min = 0;
		ctrl.max = 9007199254740991;
		ctrl.fixed = 0;
		ctrl.required = true;
		ctrl.readonly = false;
		ctrl.disabled = false;

		ctrl.selectAll = selectAll;
		ctrl.selectNone = selectNone;
		ctrl.handlerKeyDown = handlerKeyDown;
		ctrl.handlerKeyUp = handlerKeyUp;
		ctrl.blur = blur;
		ctrl.addValue = addValue;
		ctrl.addValues = addValues;
		ctrl.removeValue = removeValue;
		ctrl.isValid = isValid;

		/**
		 * L'entree est elle valid
		 * @returns {Element.validity.valid|Boolean}
		 */
		function isValid() {
			var valid = false;
			if (ctrl.fixed) {
				valid = ctrl.regex.test(ctrl.userinput);
			} else {
				valid = !$scope.ngModel.some(function (i) {
					return !isNewValueValid(i);
				});
			}
			var result = ($scope.ngModel.length) ? valid : !ctrl.required;
			ctrl.pattern = result ? '.*' : '_';
			return result;
		}

		/**
		 * Controle des entrée clavier
		 * @param {jquery event} event
		 */
		function handlerKeyDown(event) {
			var evt = event.originalEvent;
			var key = evt.key;
//			var future = ctrl.userinput.substring(0, evt.target.selectionStart)+key+ctrl.userinput.substring(evt.target.selectionEnd);
//			console.log("FUTURE", ctrl.userinput, future);
			if (!evt.ctrlKey && !ctrl.regexinput.test(key)) { // les touches de controles sont acceptées
				event.stopImmediatePropagation();
				event.stopPropagation();
				event.preventDefault();
				if ($scope.keyNav && /ArrowUp|Up|ArrowDown|Down|Add|\+/.test(key)) {
//					var selStart = evt.target.selectionStart;
//					var selEnd = evt.target.selectionEnd;
//					if (selStart === selEnd) {
//						var startinput = ctrl.userinput.substring(0, evt.target.selectionStart);
//						var endinput = ctrl.userinput.substring(evt.target.selectionEnd);
//						console.log("START '%s' END '%s', RESULT '%s'", startinput.match(/.*(\d*)$/), endinput.match(/^(\d*).*/), startinput.match(/.*(\d*)$/) + endinput.match(/^(\d*).*/));
//					}
					processUserInput(evt.target.value);
					if ($scope.ngModel && $scope.ngModel.length) {
						var old = cloneArray($scope.ngModel);
						var last = $scope.ngModel[$scope.ngModel.length - 1];
						if (/ArrowUp|Up/.test(key)) {
							ArrowUpPress(last);
						} else if (/ArrowDown|Down/.test(key)) {
							ArrowDownPress(last);
						} else if (/Add|\+/.test(key)) {
							PlusPress(last);
						}
						$scope.ngModel = distinctElements($scope.ngModel);
						updateUserInput();
//						evt.target.setSelectionRange(selStart, selEnd);
//						console.log(evt.target, evt.target.selectionStart, evt.target.selectionEnd);
						notifyChange($scope.ngModel, old);
					}
				}
			}
		}
		/**
		 * On apui sur la touche UP
		 * @param {type} last
		 */
		function ArrowUpPress(last) {
			if (isNewValueValid(last + 1)) {
				if (isRange()) {
					$scope.ngModel.push(last + 1); // c'est une plage on ajoute une valeur
				} else {
					$scope.ngModel[$scope.ngModel.length - 1]++; // Ce n'est pas une plage on incremente la derniere valeur.
				}
			}
		}
		/**
		 * On apui sur la touche DOWN
		 * @param {type} last
		 */
		function ArrowDownPress(last) {
			if (isRange()) {
				$scope.ngModel.pop(); // c'est une plage on supprime une valeur
			} else if (isNewValueValid(last - 1)) {
				$scope.ngModel[$scope.ngModel.length - 1]--; // Ce n'est pas une plage on decremente la derniere valeur.
			}
		}
		/**
		 * On apuis sur la touche PLUS
		 * @param {type} last
		 */
		function PlusPress(last) {
			var val = last + 1;
			if (isRange()) {
				val++; // si c'est une plage, on saute une valeur 
			}
			if (isNewValueValid(val)) {
				$scope.ngModel.push(val);
			}
		}
		/**
		 * 
		 * @returns {boolean}
		 */
		function isRange() {
			return $scope.ngModel.length > 1 && $scope.ngModel[$scope.ngModel.length - 2] + 1 === $scope.ngModel[$scope.ngModel.length - 1];
		}
		/**
		 * test si les bornes sont définit si la valeur est valid
		 * @param {type} val
		 * @returns {boolean}
		 */
		function isNewValueValid(val) {
			return val >= ctrl.min && val <= ctrl.max;
		}
		/**
		 * sur le keyup on valide l'entrée puis la transforme
		 * @param {jquery event} event
		 */
		function handlerKeyUp(event) {
			var evt = event.originalEvent;
			var key = evt.key;
			if (ctrl.fixed) {
				var re = /-|ArrowUp|Up|ArrowDown|Down|Add|\+/;
				if (!$scope.keyNav || !re.test(key)) {
					processUserInput(evt.target.value);
				} else {
					event.stopImmediatePropagation();
					event.stopPropagation();
					event.preventDefault();
				}
			} else {
				var re = /\d|-|Backspace|ArrowUp|Up|ArrowDown|Down|Add|\+/;
				if (!evt.target.value.length) {
					processUserInput(evt.target.value);
				} else if (!$scope.keyNav || !re.test(key)) {
					processUserInput(evt.target.value, ctrl.separators.indexOf(key) !== -1);
				} else if (/.*\d[^\d]$/.test(evt.target.value)) {
					processUserInput(evt.target.value, ctrl.separators.indexOf(key) !== -1);
				} else if (/Enter/.test(key)) {
					processUserInput(evt.target.value);
				} else {
					event.stopImmediatePropagation();
					event.stopPropagation();
					event.preventDefault();
				}
			}
		}
		function blur(event) {
			var evt = event.originalEvent;
			if (!ctrl.fixed) {
				processUserInput(evt.target.value.trim());
			}
		}
		/**
		 * Ajoute les ints
		 * @param {[int]} vals
		 */
		function addValues(vals) {
			processUserInput($filter("intstoranges")(vals || [], ctrl.fixed, ctrl.separator));
		}
		/**
		 * valid et transforme la chaine en argument
		 * @param {string} val
		 * @param {string} fromsep
		 */
		function processUserInput(val, fromsep) {
			if (val.length) {
				if (ctrl.regex.test(val)) {
					processValidUserInput(val, fromsep);
				}
			} else {
				var old = cloneArray($scope.ngModel);
				$scope.ngModel.splice(0, $scope.ngModel.length);
				updateUserInput();
				notifyChange($scope.ngModel, old);
			}
		}
		/**
		 * Traite les chaine valides
		 * @param {type} val
		 * @param {boolean} fromsep
		 */
		function processValidUserInput(val, fromsep) {
			var tmp = [];
			var sep = new RegExp("[" + ctrl.separators + "]", "g");
			var grps = val.split(sep);
			grps.forEach(function (grp) {
				if (grp.indexOf('-') !== -1) {
					var bounds = grp.split('-');
					tmp.push.apply(tmp, createRange(parseInt(bounds[0].trim()), parseInt(bounds[1].trim())).filter(function (val) {
						return isNewValueValid(val);
					}));
				} else {
					var val = parseInt(grp.trim());
					if (!isNaN(val) && isNewValueValid(val)) {
						tmp.push(val);
					}
				}
			});
			tmp = distinctElements(tmp);
			tmp = tmp.sort();
			var old = cloneArray($scope.ngModel);
			$scope.ngModel.splice(0, $scope.ngModel.length);
			Array.prototype.push.apply($scope.ngModel, tmp);
			updateUserInput(fromsep);
			notifyChange($scope.ngModel, old);
		}
		/**
		 * utilisateur à clicker sur selectall
		 */
		function selectAll() {
			var old = cloneArray($scope.ngModel);
			$scope.ngModel.splice(0, $scope.ngModel.length);
			Array.prototype.push.apply($scope.ngModel, ctrl.values);
			updateUserInput();
			notifyChange($scope.ngModel, old);
		}
		/**
		 * utilisateur à clicker sur selectnone
		 */
		function selectNone() {
			var old = cloneArray($scope.ngModel);
			$scope.ngModel.splice(0, $scope.ngModel.length);
			updateUserInput();
			notifyChange($scope.ngModel, old);
		}
		/**
		 * Utilisateur à clicker sur une valeur pour l'ajouter
		 * @param {int} val
		 */
		function addValue(val) {
			var old = cloneArray($scope.ngModel);
			$scope.ngModel.push(val);
			updateUserInput();
			notifyChange($scope.ngModel, old);
		}
		/**
		 * Utilisateur à clicker sur une valeur pour la supprimer
		 * @param {int} val
		 */
		function removeValue(val) {
			var old = cloneArray($scope.ngModel);
			$scope.ngModel.splice($scope.ngModel.indexOf(val), 1);
			updateUserInput();
			notifyChange($scope.ngModel, old);
		}
		/**
		 * Met à jour la chaine dans le input
		 * @param {boolean} withend
		 */
		function updateUserInput(withend) {
			ctrl.userinput = $filter("intstoranges")($scope.ngModel, ctrl.fixed, ctrl.separator, withend);
		}
		/**
		 * throw event 
		 * @param {type} newValue
		 * @param {type} oldValue
		 */
		function notifyChange(newValue, oldValue) {
			if (!ctrl.disabled && $scope.ngChange && !arraysEqual(newValue, oldValue)) {
				$scope.ngChange({"data": newValue, old: oldValue});
			}
		}
	}
	function createRange(bound1, bound2) {
		return __.range(Math.min(bound1, bound2), Math.max(bound1, bound2) + 1);
	}
	function cloneArray(arr) {
		return __.clone(arr);
	}
	function arraysEqual(arr1, arr2) {
		return __.isEqual(arr1, arr2);
	}
	function distinctElements(arr) {
		return __.uniq(arr);
	}
	/**
	 * Filtre angular permettant la mise en forme d'un array d'int en ranges
	 * @returns {Function}
	 */
	function intsToRanges() {
		return function (array, fixed, sep, withend) {
			if (!sep) {
				sep = ";";
			}
			var result = "";
			if (array) {
				var sorted = array.sort(function (a, b) {
					return a - b;
				});
				var pval = 0;
				var ranged = false;
				sorted.forEach(function (item, idx, arr) {
					var val = item;
					if (!result.length) {
						result += toFixed(val, fixed);
					} else if (val === pval + 1) {
						if (!ranged) {
							result += "-";
							ranged = true;
						}
					} else {
						if (ranged) {
							result += toFixed(pval, fixed);
						}
						result += sep + " " + toFixed(val, fixed);
						ranged = false;
					}
					pval = val;
				});
				if (ranged) {
					result += toFixed(pval, fixed);
				}
			}
			return result + (withend ? sep : "");
			function toFixed(val, fixed) {
				if (isNaN(val))
					return "";
				return fixed ? ("0000000" + val).slice(-fixed) : "" + val;
			}
		};
	}
	/**
	 * filtre permettant d'extraire un array d'un autre
	 * arr1|without:arr2 
	 * @returns {Function}
	 */
	function without() {
		return function (array, without) {
			var result = [];
			if (array) {
				if (without) {
					array.forEach(function (item) {
						if (without.indexOf(item) === -1) {
							result.push(item);
						}
					});
				} else {
					result = array;
				}
			}
			return result;
		};
	}
})(angular, _);
